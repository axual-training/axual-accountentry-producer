package io.axual.training.producer;

import io.axual.payments.*;
import io.axual.training.common.producer.ProducerProperties;
import org.apache.kafka.clients.producer.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SimpleRecordProducer {
    private static final Logger LOG = LoggerFactory.getLogger(SimpleRecordProducer.class);
    private Producer<AccountId, AccountEntryEvent> producer = null;

    public SimpleRecordProducer(@Autowired ProducerProperties producerProperties) {
        LOG.info("Creating Kafka Producer with properties {}", producerProperties.asProperties());
        producer = new KafkaProducer<>(producerProperties.asProperties());
    }

    @Scheduled(fixedRate = 500)
    public void run() {
        /*
            This method is called every 500 milliseconds.
            Create one or more AccountEntry messages using the AccountId as key and AccountEntry as value
            TIP: use the components in this repo, and don't forget to add a callback to the send() method of the Producer so you have some feedback

            more info: https://kafka.apache.org/20/javadoc/index.html?org/apache/kafka/clients/producer/KafkaProducer.html
            You can compile this application with the command: mvn compile package
            You can run this application with the command: mvn spring-boot:run
         */
    }
}
